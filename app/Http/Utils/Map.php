<?php

namespace App\Http\Utils;

class Map
{
    /**
     * 地图关键字搜索
     * @param string $keyword 关键字
     * @param string|null $region 省份/城市/区
     * @param int $page 页数
     * @param int $per_page 行数
     * @return array|false
     */
    public static function getMapSuggestion(string $keyword,?string $region=null,?string $latitude=null,?string $longitude=null,int $page=1,int $per_page=20){
        $url="https://apis.map.qq.com/ws/place/v1/suggestion/?keyword=".$keyword."&key=".env('TENCENT_MAP_API_KEY');
        if($region){
            $url.="&region=".$region;
        }
        if($latitude&&$longitude){
            $url.="&location=".$latitude.",".$longitude;
        }
        //var_dump($url);

        $url.="&page=".$page."&page_size=".$per_page;
        $res=curl_get($url);
        if($res['data']){

            $res['last_page']=ceil($res['count']/$per_page);
            $res['current_page']=$page;
            $res['total']=$res['count'];
        }
        return $res;
    }

    /**
     * 获取经纬度描述
     * @param string $latitude 纬度
     * @param string $longitude 经度
     * @return mixed|null
     */
    public static function latLongDesc(string $latitude,string $longitude){
        $url="https://apis.map.qq.com/ws/geocoder/v1/?location={$latitude},{$longitude}&key=".env("TENCENT_MAP_API_KEY")."&get_poi=1";
        $res=curl_get($url);
        if(isset($res['result'])){
            if($res['result']['address_component']){
                if($res['result']['address_component']['street']===$res['result']['address_component']['street_number']){
                    $res['result']['address_component']['street_number']='';
                }
            }
            if($res['result']['formatted_addresses']&&$res['result']['formatted_addresses']['recommend']){
                if(stripos($res['result']['formatted_addresses']['recommend'],$res['result']['address'])===false){
                    $res['result']['address']=$res['result']['address_component']['province']
                            .$res['result']['address_component']['city']
                            .$res['result']['address_component']['district']
                            .$res['result']['address_component']['street']
                            .$res['result']['formatted_addresses']['recommend'];
                }
                if($res['result']['formatted_addresses']){
                    if(stripos($res['result']['formatted_addresses']['recommend'],$res['result']['address_component']['street_number'])===false&&stripos($res['result']['formatted_addresses']['recommend'],$res['result']['address_component']['street'])===false){
                        $res['result']['address_component']['street_number']=$res['result']['formatted_addresses']['recommend'];
                    }
                }
            }
            if($res['result']['ad_info']&&$res['result']['ad_info']['adcode']){
                $res['result']['city_code']=substr($res['result']['ad_info']['adcode'],0,4);
            }
            return $res['result'];
        }else{
            return $res;
        }

    }
}
