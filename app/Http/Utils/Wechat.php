<?php
namespace App\Http\Utils;

use EasyWeChat\Factory;

class Wechat
{
    /**
     * 新版本code交换手机号
     * @param string $code
     * @param string|null $token
     */
    public static function getPhoneByCode(string $code,?string $token=null){

        if (empty($token)){
            $res=Factory::miniProgram(config('wechat.mini_program.default'))->access_token->getToken();
            $token=$res['access_token']??$token;
        }
        $url="https://api.weixin.qq.com/wxa/business/getuserphonenumber?access_token=".$token;
        return curl_post($url,['code'=>$code],['Content-type: application/json'],'json');
    }
}
