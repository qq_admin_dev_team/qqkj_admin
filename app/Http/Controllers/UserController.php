<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Overtrue\LaravelWeChat\Facade;

class UserController extends Controller
{
    /**
     * 微信小程序授权登录
     * @param Request $request
     */
    public function miniappLogin(Request $request): \Illuminate\Http\JsonResponse
    {
        try{
            $app=Facade::miniProgram(config('wechat.mini_program.default'));
            $res=$app->auth->session($request->input("code"));
            if(isset($res['openid'])){
                //登录逻辑
                $session=session(['session_key'=>$res['session_key']]);
            }
            $token=jwt_encode(['user_id'=>1,'role'=>1]);//保存关键信息即可
            return show_success("登录成功",[
                "token"=>$token,
            ]);
        }catch(\Exception $e){
            return show_error('请求异常',[$e->getMessage()]);
        }
    }

    /**
     * 小程序解密数据
     * @param Request $request
     */
    public function decryptData(Request $request){
        try{
            $app=Facade::miniProgram(config('wechat.mini_program.default'));
            // api中使用session需要在Kernel.php中开启
            $session=session("session_key");//可以使用登录时候的sessionkey，也可以让前端重新传code获取，注意
            $decryptedData = $app->encryptor->decryptData($session,$request->input("iv"), $request->input("encryptedData"));

        }catch(\Exception $e){

        }
    }
    /**
     * 公众号授权回调
     * @param Request $request
     */
    public function officialLogin(Request $request){
        $app=Facade::officialAccount(config('wechat.official_account.default'));
        $user=$app->oauth->user();
        // $user->getId();  // 对应微信的 OPENID
        // $user->getNickname(); // 对应微信的 nickname
        // $user->getName(); // 对应微信的 nickname
        // $user->getAvatar(); // 头像网址
        // $user->getOriginal(); // 原始API返回的结果
        //登录处理完了调回前端地址
        header("location:".env("APP_URL")."/h5/#/");

    }
}
