<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yansongda\Pay\Pay;

class HomeController
{

    public function index(Request $request){

    }

    /**
     * 微信支付回调v3
     * @param Request $request
     */
    public function notify(Request $request){
        $data=Pay::wechat(config("pay"))->callback();
        if(env("APP_DEBUG")){
          //  Log::info("支付成功回调参数",json_decode(json_encode($data,JSON_UNESCAPED_UNICODE),true));
        }
        if($data->event_type=="TRANSACTION.SUCCESS"){
            $pay_no= $data->resource['ciphertext']['out_trade_no'];
            $pay_money=bcdiv($data->resource['ciphertext']['amount']['total'],100,2);
        }
    }

    /**
     * 微信小程序支付v3
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pay(Request $request){
        $data = [
            'out_trade_no' => time()."",
            'description' => "购买商品",
            'notify_url'=>env('APP_URL').'/api/notify/wechat',
            'amount' => [
                'total' => intval( 1*100),
            ],
            'payer' => [
                'openid' => 'openid_openid_openid_openid',
            ],
        ];
        $res=Pay::wechat(config("pay"))->mini($data);
        return show_success('支付测试',$res);
    }
}
