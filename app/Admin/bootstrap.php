<?php

use Dcat\Admin\Admin;
use Dcat\Admin\Grid;
use Dcat\Admin\Form;
use Dcat\Admin\Grid\Filter;
use Dcat\Admin\Show;

/**
 * extend custom field:
 * Dcat\Admin\Form::extend('php', PHPEditor::class);
 * Dcat\Admin\Grid\Column::extend('php', PHPEditor::class);
 *
 * Or require js and css assets:
 * Admin::css('/packages/prettydocs/css/styles.css');
 * Admin::js('/packages/prettydocs/js/main.js');
 *
 */
//Dcat\Admin\Form::extend('tags', \App\Admin\Extensions\Tags::class);
Grid::resolving(function (Grid $grid) {
    $grid->disableViewButton();
    $grid->disableRowSelector();
    $grid->disableRefreshButton();
    $grid->withBorder(true);
});
Form::resolving(function (Form $form){
    $form->disableViewButton();
    $form->disableEditingCheck();
    $form->disableCreatingCheck();
});

