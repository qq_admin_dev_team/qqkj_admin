<?php

namespace App\Admin\Actions;

use App\Admin\Forms\OrderSendForm;
use Dcat\Admin\Contracts\LazyRenderable;
use Dcat\Admin\Grid\RowAction;
use Dcat\Admin\Traits\LazyWidget;
use Dcat\Admin\Widgets\Modal;

class OrderSendAction extends RowAction implements LazyRenderable {
	use LazyWidget; // 使用异步加载功能
	protected $model;
	protected $title = '<span style="padding:0px 10px;padding-right:50px"><i class="feather icon-twitter"></i> 发货</span>';

	public function render() {
		// 实例化表单类并传递自定义参数
		$form = OrderSendForm::make()->payload(['id' => $this->getKey()]);

		return Modal::make()
			->lg()
			->title($this->title)
			->body($form)
			->button($this->title);
	}
}
