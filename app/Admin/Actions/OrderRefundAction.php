<?php

namespace App\Admin\Actions;

use Dcat\Admin\Grid\RowAction;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class OrderRefundAction extends RowAction {
	protected $model;
	protected $title = '<span style="padding:0px 10px"><i class="feather icon-zap"></i> 退款</span>';


	/**
	 * 设置确认弹窗信息，如果返回空值，则不会弹出弹窗
	 *
	 * 允许返回字符串或数组类型
	 *
	 * @return array|string|void
	 */
	public function confirm() {
		return [
			// 确认弹窗 title
			"是否进行退款？",
			// 确认弹窗 content
			$this->row->username,
		];
	}

	/**
	 * 处理请求
	 *
	 * @param Request $request
	 *
	 * @return \Dcat\Admin\Actions\Response
	 */
	public function handle(Request $request) {
		// 获取当前行ID
		$id = $this->getKey();

		// 获取 parameters 方法传递的参数
		$username = $request->get('username');
		$model = $request->get('model');

		// 复制数据
		$model::find($id)->replicate()->save();

		// 返回响应结果并刷新页面
		return $this->response()->success("复制成功: [{$username}]")->refresh();
	}

	/**
	 * 设置要POST到接口的数据
	 *
	 * @return array
	 */
	public function parameters() {
		return [
			// 发送当前行 username 字段数据到接口
			'username' => $this->row->username,
			// 把模型类名传递到接口
			'model' => $this->model,
		];
	}
}
