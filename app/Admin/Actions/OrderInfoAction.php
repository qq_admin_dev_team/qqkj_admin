<?php

namespace App\Admin\Actions;

use Dcat\Admin\Admin;
use Dcat\Admin\Grid\RowAction;
use Illuminate\Database\Eloquent\Model;

class OrderInfoAction extends RowAction {
	protected $model;
	protected $title = '<span style="padding:0px 10px"><i class="feather icon-file-text"></i> 详情</span>';


	/**
	 * 添加JS
	 *
	 * @return string
	 */
	protected function script() {
		$id = $this->getRow()->get("id");
		$url = admin_url("/order/detail") ;
		return <<<JS
$(".grid-check-row").click(function(){
	layer.open({
		type:2,
		title: '<h2 style="margin-top:10px">订单详情 - {$this->row->order_no}</h2>',
		content: "{$url}",
		selector: '#form-order-detail-'+{$id},
		maxItem: 3,
		area: ["80%","80%"],
		queryName: '_resource_',
		items: {},
		showCloseButton: false,
	});
})

JS;
	}

	public function html() {
		// 获取当前行数据ID
		$id = $this->getKey();

		// 这里需要添加一个class, 和上面script方法对应
		$this->setHtmlAttribute(['data-id' => $id, 'data-order' => $this->row->order_no, 'class' => 'grid-check-row']);

		return parent::html();
	}
}
