<?php

namespace App\Admin\Actions;

use Dcat\Admin\Admin;
use Dcat\Admin\Grid\Tools\AbstractTool;

class ImportTool extends AbstractTool {
    protected $url = "";
    public function __construct($url) {

        $this->url = $url;
    }
    protected function script() {

// 		return <<<JS
        // 	//
        // });
        // JS;
    }

    public function render() {
        Admin::script($this->script());

        return view('admin.tools.import', ['url' => $this->url]);
    }
}
