<?php

namespace App\Admin\Controllers;

use App\Admin\Actions\ImportTool;
use App\Admin\Repositories\Order;
use App\Admin\Actions;
use App\Models\Order as OrderModel;
use App\Models\OrderDetail;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Show;

class OrderController extends AdminController {

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid() {
        return Grid::make(new Order(['user']), function (Grid $grid) {
            $grid->quickSearch('order_no','user.nickname')->placeholder('搜索订单号，用户昵称'); //快速搜索框
            $grid->model()->orderBy("id", 'desc'); //修改排序
            $grid->disableRefreshButton(); //禁用刷新按钮
            $grid->disableRowSelector(); //取消全选
            $grid->disableDeleteButton(); //禁用删除
            $grid->disableViewButton(); //禁用详情按钮
          //  $grid->disableEditButton(); //禁用编辑按钮
            $grid->disableCreateButton(); //禁用新增
            $grid->column('order_no');
            $grid->column('contact');
            $grid->column('mobile');
            $grid->column('city');
            $grid->column('pay_type')->using([
                0 => '已取消',
                1 => '待支付',
                2 => '待发货',
                3 => '待收货',
                4 => '已完成',
            ]);
            $grid->column('money');
            $grid->column('status')->using([
                0 => '已取消',
                1 => '待支付',
                2 => '待发货',
                3 => '待收货',
                4 => '已完成',
            ])->badge([
                'default' => 'danger', // 设置默认颜色，不设置则默认为 default
                0 => 'danger',
                1 => 'danger',
                2 => 'warning',
                3 => 'primary',
                4 => 'success',
            ]);
            $grid->column('created_at');
            //数据导入
            $grid->tools(new ImportTool('/order/import'));
            //订单导出
            $grid->export()->rows(function (array $rows) {
                return $rows;
            });
            $grid->actions(function (Grid\Displayers\Actions $actions) {
                // 当前行的数据数组
                $rowArray = $actions->row->toArray();
                // 当前行的某个字段的数据
                $status = $actions->row->status;
                if($status>0){//状态大于0显示一个操作按钮
                    //表格弹出工具表单
                    $actions->append(new Actions\OrderSendAction());
                }
            });
            //表格弹出自定义页面
            $grid->actions([new Actions\OrderInfoAction()]);
            //表格弹出确认对话框
            $grid->actions([new Actions\OrderRefundAction()]);

            $grid->filter(function (Grid\Filter $filter) {
                $filter->like('order_no');
                $filter->equal("status")->select([
                    '0' => "已取消",
                    "1" => "待支付",
                    "2" => "待发货",
                    "3" => "待收货",
                    "4" => "已完成",
                ]);
                $filter->like('contact');
                $filter->like('mobile');
                $filter->like('created_at')->date();
            });
        });
    }

    /**
     * 自定义订单详情页面
     * @param Content $content
     * @return Content
     */
    protected function orderDetail(Content $content){
        $order = OrderModel::where("id", request()->input("id", 0))->first();
        if (!empty($order)) {
            return $content->full()//渲染不带菜单框架，只有样式文件的页面
            ->body(view("admin.order.detail",
                ['order' => $order]
            ));

        } else {
            //abort(404);
        }
    }
    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    public function info(Content $content) {
        $order = OrderModel::where("id", request()->input("id", 0))->first();

        if (!empty($order)) {
            $goods = OrderDetail::where("order_no", $order->order_no)->get();

            return $content->full()->body(view("admin.order.detail", ['order' => $order, 'goods' => $goods]));
        } else {
            //abort(404);
        }

    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form() {
        return Form::make(new Order(), function (Form $form) {
            $form->display('id');
            $form->text('order_no');
            $form->tags("city",'标签');

            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
