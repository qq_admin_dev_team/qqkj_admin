<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Dcat\Admin\Admin;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Traits\HasUploadedFile;

class HomeController extends Controller
{
    use HasUploadedFile;
    /**
     * 首页
     * @author MrGuo
     * @date   2020-12-25
     * @param  Content
     */
    public function index(Content $content): Content
    {
        Admin::js("/vendor/libs/echarts/echarts.min.js");
        $data=[
            'total_user'=>1
        ];
        return $content
            ->header('首页')
            ->description('')
            ->body(view("admin.home.index",$data));
    }

    /**
     * 后台文件上传
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function upload() {
        $driver = env("FILESYSTEM_DRIVER", 'public');
        $disk = $this->disk($driver);
        // 判断是否是删除文件请求
        if ($this->isDeleteRequest()) {
            // 删除文件并响应
            return $this->deleteFileAndResponse($disk);
        }
        if (request()->hasFile('file')) {
            $file = request()->file("file");
        } else {
            $file = request()->file("_file_");
        }
        if (empty($file)) {
            return $this->responseErrorMessage('未检测到文件');
        }
        //$md5 = md5(md5_file($file) . time() . mt_rand(1000, 9999));//大文件消耗性能
        $md5 = md5(unique_no("FILES_", time()));
        //暂时取消 同一文件不覆盖
        // $exist = $model->where("md5", $md5)->first();
        // if (!empty($exist)) {
        //     return $this->responseUploaded($exist->path, $disk->url($exist->path));
        // }
        // 获取上传的字段名称
        $ext = strtolower($file->getClientOriginalExtension());
        $allows = config("filesystems.limit_ext.allows");
        $dir = "";
        foreach ($allows as $key => $val) {
            if (in_array($ext, $val)) {
                $dir = $key;
            }
        }
        if ($dir === "" && config("filesystems.limit_ext.enable")) {
            return $this->responseErrorMessage("不允许上传的文件类型！");
        }
        if ($dir === "") {
            $dir = "file";
        }
        $newName = date("Ym") . "/" . $md5 . "." . $file->getClientOriginalExtension();
        try {
            $result = $disk->putFileAs("uploads/" . $dir . "s", $file, $newName);
            $path = "/uploads/" . $dir . "s/" . $newName;
            if ($result) {
                //暂时不插入数据库，
                // $id = AdminAttachment::insert([
                // 	'module' => $request->input("module", "admin"),
                // 	'path' => $path,
                // 	'type' => $dir,
                // 	'url' => $disk->url($path),
                // 	'md5' => $md5,
                // 	'name' => $file->getClientOriginalName(),
                // 	'driver' => $driver,
                // 	'created_at' => date("Y-m-d H:i:s"),
                // 	'updated_at' => date("Y-m-d H:i:s"),
                // ]);
            }

            return $result
                ? $this->responseUploaded(env("UPLOAD_FULL_URL") ? $disk->url($path) : $path, $disk->url($path))
                : $this->responseErrorMessage('文件上传失败');
        } catch (\Excetion $e) {
            $this->responseErrorMessage('文件上传失败, ' . $e->getMessage());
        }

    }

}
