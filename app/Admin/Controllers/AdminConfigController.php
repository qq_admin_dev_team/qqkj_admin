<?php

namespace App\Admin\Controllers;

use App\Models\AdminConfig;
use App\Admin\Forms\AdminConfig as ConfigForm;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Http\Controllers\AdminController;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Show;
use Dcat\Admin\Widgets\Alert;
use Dcat\Admin\Widgets\Card;

class AdminConfigController extends AdminController {
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid() {

        return Grid::make(new AdminConfig(), function (Grid $grid) {
            //$grid->disableToolbar();
            $grid->disableRefreshButton();
            $grid->disableFilterButton();
            $grid->model()
                ->orderBy("sort", 'asc')
                ->orderBy("id", 'desc');
            $grid->quickSearch('title', 'group', 'name')->placeholder('搜索名称，键名，分组');
            $grid->header(function ($collection) {
                // 自定义组件
                $alert = Alert::make('非开发人员不要操作', '警告');

                // 类型
                $alert->error();
                return $alert;
            });
            $grid->id;
            $grid->title;
            $grid->name;

            $grid->group;
            $grid->type;
            $grid->sort->sortable()->editable();
            $grid->status->using([1 => '启用', 0 => '禁用'])->badge([
                'default' => 'warning', // 设置默认颜色，不设置则默认为 default
                1 => 'success',
            ]);
            $grid->updated_at;
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
                $filter->equal('name');
                $filter->equal('title');
            });
        });
    }
    /**
     * 参数设置
     */
    protected function setItems(Content $content) {
        return $content->title('系统设置')
            ->body(new Card(new ConfigForm()));
    }
    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id) {

        return Show::make($id, new AdminConfig(), function (Show $show) {
            $show->id;
            $show->name;
            $show->title;
            $show->group;
            $show->type;
            $show->value;
            $show->options;
            $show->tips;
            $show->format;
            $show->create_at;
            $show->update_at;
            $show->sort;
            $show->status;
            $show->delete_at;
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form() {
        return Form::make(new AdminConfig(), function (Form $form) {
            $form->display('id');
            if ($form->model()->id) {
                $form->text('name')->help("由英文字母和下划线组成，如 <code>site_copyright</code>，调用方法：<code>admin_config('site_copyright')</code>")
                    ->placeholder("创建后不可修改")
                    ->disable();
            } else {
                $form->text('name')->help("由英文字母和下划线组成，如 <code>site_copyright</code>，调用方法：<code>admin_config('site_copyright')</code>")
                    ->placeholder("创建后不可修改")
                    ->rules("required|unique:admin_config");
            }

            $form->text('title')->rules("required");
            $form->select('type')->rules('required')
                ->options(AdminConfig::getInputType());

            $form->select('group')
                ->options(AdminConfig::getConfigGroup());
            $form->textarea('options')
                ->help("格式: value:label , 每行一个, 英文逗号分隔")
                ->placeholder("单选多选的选项")
                ->rules('max:255');
            $form->text('placeholder');
            $form->text('help');
            $form->text('rules')->placeholder("Laravel表单验证规则，例如：numeric|between:0,100");
            $form->text('sort')->default(100);
            $form->saved(function (Form $form, $result) {
                // 保存问了以后更新缓存中的数据
                AdminConfig::loadConfig($form->model()->name);
            });
        });
    }
}
