<?php

namespace App\Admin\Repositories;

use App\Models\AdminConfig as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class AdminConfig extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
