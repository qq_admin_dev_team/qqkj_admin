<?php

namespace App\Admin\Forms;

use App\Models\ShopExpress;
use App\Models\Order;
use App\Models\OrderDetail;
use Dcat\Admin\Contracts\LazyRenderable;
use Dcat\Admin\Traits\LazyWidget;
use Dcat\Admin\Widgets\Form;

class OrderSendForm extends Form implements LazyRenderable {
	use LazyWidget; // 使用异步加载功能

	// 处理请求
	public function handle(array $input) {
		// 获取外部传递参数
		$id = $this->payload['id'] ?? null;
		$order = Order::where("id", $id)->first();
		if ($order->status != 2 && $order->status != 2) {
			return $this->response()->error("订单状态不允许发货");
		}
		// 表单参数

		return $this->response()->success('密码修改成功');
	}

	public function form() {
		// 获取外部传递参数
		$id = $this->payload['id'] ?? null;
		$order = Order::where("id", $id)->first();
		$this->display('order_no', "订单号")->value($order->order_no);
		$this->select('express_type', "快递公司")->options(['sf'=>'顺丰','zt'=>"中通",'yto'=>'圆通'])->required();
		$this->text("express_no", '物流单号')->required();
		if (!empty($order)) {

			$this->html("<p>商品A * 1</p><p>商品B * 10</p>", '商品清单');
		}

		$this->textarea('message', "买家留言")->readonly()->rows(2)->placeholder("暂无")->value($order->message);
	}
}
