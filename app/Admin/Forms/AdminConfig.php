<?php

namespace App\Admin\Forms;

use App\Models\AdminConfig as AdminConfigModel;
use Dcat\Admin\Layout\Content;
use Dcat\Admin\Widgets\Form;
use Symfony\Component\HttpFoundation\Response;

class AdminConfig extends Form {
    protected $group_id = 0;

    public function index(Content $content) {
        $content->withError('Title', 'messages..');
        die();
        return $content;
    }
    /**
     * Handle the form request.
     *
     * @param array $input
     *
     * @return Response
     */
    public function handle(array $input) {
        foreach ($input as $k => $v) {
            AdminConfigModel::where("name", $k)->limit(1)->update(['value' => $v]);
            AdminConfigModel::loadConfig($k);
        }
        $fields = AdminConfigModel::orderBy("sort", "asc")
            ->orderBy("id", "asc")->get();
        $datas = [];
        return $this->response()->success('保存成功.', '/settings');
    }

    /**
     * Build a form here.
     */
    public function form() {

        $groups = AdminConfigModel::getConfigGroup();
        foreach ($groups as $group_id => $group) {

            $this->tab($group, function (Form $form) use($group_id) {
                $fields = AdminConfigModel::where("group", $group_id)
                    ->orderBy("sort", "asc")->orderBy("id", "asc")->get();

                foreach ($fields as $field) {
                    $options = [];
                    $arr = explode(",", str_ireplace("\r\n", ",", $field->options));
                    if (gettype($arr) == 'string') {
                        $arr = [$arr];
                    }

                    foreach ($arr as $key => $val) {
                        $arr2 = explode(":", $val);
                        if (gettype($arr2) == "array" && count($arr2) > 1) {
                            $options[$arr2[0]] = $arr2[1];
                        }
                    }
                    $holder = empty($field->placeholder) ? "请输入" . $field->title : $field->placeholder;
                    $type=$field->type;
                    try{
                        $form1=$form->$type($field->name,$field->title)
                            ->rules($field->rules&&$field->rules!=''?$field->rules:'required')
                            ;
                        switch ($field->type) {
                            case 'select':
                            case 'radio':
                            case 'checkbox':
                                $form1->options($options);
                                break;
                            case 'image':
                                $form1->accept('jpg,png,gif,jpeg,bmp,jpg', 'image/*')
                                    ->url("upload")
                                    ->default($field->value);
                                break;
                            case "currency":
                                $form1->symbol("￥");
                                break;
                            default:
                                # code...
                                break;
                        }
                        if($field->help){
                            $form1=$form1->help($field->help);
                        }
                        $form1->required()
                            ->placeholder($holder)->value($field->value);

                    }catch(\Exception $e){

                    }

                }
            });
        }

    }
}
