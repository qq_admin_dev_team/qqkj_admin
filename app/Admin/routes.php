<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Dcat\Admin\Admin;

Admin::routes();

Route::group([
    'prefix'     => config('admin.route.prefix'),
    'namespace'  => config('admin.route.namespace'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', 'HomeController@index');
    $router->any('/upload', 'HomeController@upload');
    $router->any('/config/settings', 'AdminConfigController@setItems');
    $router->resource('/config/list', 'AdminConfigController');

    $router->resource('/order/list', 'OrderController');
    $router->any('/order/detail', 'OrderController@orderDetail');
});
