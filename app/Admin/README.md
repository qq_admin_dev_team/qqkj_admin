##列表操作方法
~~~
$grid->quickSearch('order_no')->placeholder('搜索订单号'); //快速搜索框
$grid->model()->orderBy("id",'desc');//修改排序
$grid->disableFilterButton();	//禁用筛选条件
$grid->disableRefreshButton();	//禁用刷新按钮
$grid->disableRowSelector();	//取消全选

$grid->disableDeleteButton(); 	//禁用删除
$grid->disableViewButton(); 	//禁用详情按钮
$grid->disableEditButton(); 	//禁用编辑按钮
$grid->disableCreateButton(); 	//禁用新增

$grid->fixColumns(2);			//固定前两列与最后一列 ，页面过长时左右滚动

$grid->column('title')
    ->sortable()                //字段可以排序
    ->editable()                //允许快速编辑，和switch()都是走的表单保存，必须在form中有这个字段
    ->limit(20)                 //最多显示20个字符，超出省略号
$grid->column('image')->image(null,50,50); //图片，尺寸50*50
$grid->column('创建时间')->display(function()){ //自定义返回格式
    return substr($this->created_at,0,10);
})
$grid->column('status','订单状态')
    ->using([0=>'未支付',1=>'已支付'])
    ->badge(['default'=>'danger','1'=>'success'])       //加样式，也可用   dot
$grid->display();

$grid->export()->rows(function (array $rows) {
    foreach ($rows as $index => &$row) {
        $row['name'] = $row['first_name'].' '.$row['last_name'];
    }

    return $rows;
}); 

//数据导出
$grid->export()
    ->titles(['title'=>'文章标题','created_at'=>'发布时间'])    //自定义表头
    ->filename('租赁订单')  //自定义文件名
    ->rows(function (array $rows) {         //修改数据
        foreach ($rows as $index => &$row) {
            $row['name'] = $row['first_name'].' '.$row['last_name'];
        }
    
        return $rows;
    })  
//导入，相关文件：/app/Admin/Extensions/Excel/ImoportTool.php  /resources/views/admin/tool/import.blade.php
$grid->tools(new ImportTool('/user/import'));
~~~

##表单操作方法
~~~
$form->text("name")
    ->rules("between:0,9999")   //自定义验证规格，
    ->required()                //必填
    ->help()                    //下方提示语
    ->readonly()                //只读
    ->disable()                 //禁用
    ->maxLength(100)            //最大长度，仅text可用
    
//选择框：select , radio , checkbox
$form->radio("status")
    ->default(1)            //默认值
    ->saveAsJson()          //保存为json字符串
    ->saving(function ($value) {
        // 转化逗号拼接字符串保存到数据库
        return implode(',',$value);
    });
    ->options([1=>'上架',0=>'下架‘)
    ->when([1],function(Form $form){
        //当选中1的时候显示
    });

//上传
$form->image('logo')
    ->disk('oss');          //指定文件驱动
    ->maxSize(1024)         //文件大小限制（KB）
    ->saveFullUrl()         //是否保存域名，只对默认上传接口生效，自定义url不行
    ->url('/upload')        //自定义上传接口
$form->file('file')         
    ->accept('jpg,png,gif,jpeg') //限制文件类型
// 限制最大上传数量
$form->multipleImage("banners")
    ->sortable();           //可进行排序
    ->limit(10);            //限制数量
    ->saving(function ($paths) {    //自定义保存格式
        // return implode(',', $paths); 
        return json_encode($paths);
    }); 

//地图选点
$form->map("latitude","longitude","门店位置");

//
$form->currency('price')->symbol('￥');

if($form->isCreating()){//表单是否是新增操作

}

if($form->isDeleting()){//表单是否是删除操作
    
}

$form->deleting(function(Form $form){//表单删除前
    $dataArr = $form->model()->toArray();   //这里是取到的数组
    foreach($dataArr as $item){
        if($item['status']>0){
            return $form->response()->error('订单已支付，无法删除');//中断删除
        }
    }
});
$form->saving(function(Form $form){//表单修改保存前
    if($form->model()->status<1){
    
    }
});
$form->saved(function(Form $form){//表单修改保存后
    if($form->model()->status==1){
        
    }
});
~~~
