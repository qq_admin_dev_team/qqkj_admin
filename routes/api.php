<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//使用跨域中间件，跨域配置/config/cors.php
Route::group(['middleware' => [], 'prefix' => 'home'], function () {
    Route::any('xxxx', 'HomeController@index');
});

//使用自定义token中间件
Route::group(['middleware' => ['cors','auth.token'], 'prefix' => 'attachment'], function () {
    Route::post('xxxx', 'MatchController@commentPublish');
});
