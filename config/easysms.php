<?php
return  [
    //这个templates是我自己加的
    "templates"=>[
        'login'=>[
            'template_code'=>'SMS_179880153',
            'content'=>'验证码${code}，您正在进行身份验证，打死不要告诉别人哦！'
        ],
        'confirm'=>[
            'template_code'=>'SMS_179880153',
            'content'=>'验证码${code}，您正在进行身份验证，打死不要告诉别人哦！'
        ],
        'regist'=>[
            'template_code'=>'SMS_179880150',
            'content'=>'验证码${code}，您正在注册成为新用户，感谢您的支持'
        ],
        'changeMobile'=>[
            'template_code'=>'SMS_179880148',
            'content'=>'验证码${code}，您正在尝试变更重要信息，请妥善保管账户信息。'
        ],
        'resetPwd'=>[
            'template_code'=>'SMS_179880149',
            'content'=>'验证码${code}，您正在尝试修改登录密码，请妥善保管账户信息。'
        ]
    ],
    // HTTP 请求的超时时间（秒）
    'timeout' => 5.0,
    // 默认发送配置
    'default' => [
        // 网关调用策略，默认：顺序调用
        'strategy' => \Overtrue\EasySms\Strategies\OrderStrategy::class,
        // 默认可用的发送网关
        'gateways' => [
            'aliyun','qcloud'
        ],
    ],
    // 可用的网关配置
    'gateways' => [
        'errorlog' => [
            'file' => '/tmp/easy-sms.log',
        ],
        'aliyun' => [
            'access_key_id' =>env("ALIYUN_ACCESS_ID"),
            'access_key_secret' => env('ALIYUN_ACCESS_KEY_SECRET'),
            'sign_name' =>env('ALIYUN_SIGN_NAME'),
        ],
    ]
];
