<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | following language lines contain default error messages used by
    | validator class. Some of these rules have multiple versions such
    | as size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => ':attribute 必须是有效值.',
    'active_url' => ':attribute 不是有效链接.',
    'after' => ':attribute 必须在大于 :date ',
    'after_or_equal' => ':attribute 必须大于或等于 :date ',
    'alpha' => ':attribute may only contain letters.',
    'alpha_dash' => ':attribute may only contain letters, numbers, dashes and underscores.',
    'alpha_num' => ':attribute may only contain letters and numbers.',
    'array' => ':attribute 必须是数组.',
    'before' => ':attribute 必须在 :date 之前.',
    'before_or_equal' => ':attribute 必须在 :date 之前.',
    'between' => [
        'numeric' => ':attribute 必须在 :min 到 :max 之间',
        'file' => ':attribute 必须在 :min 到 :max Kb.',
        'string' => ':attribute 必须 :min 到 :max 个字符.',
        'array' => ':attribute 必须选中 :min 到 :max 项.',
    ],
    'boolean' => ':attribute 必须为 true 或 false.',
    'confirmed' => ':attribute confirmation does not match.',
    'date' => ':attribute 必须是有效日期.',
    'date_equals' => ':attribute must be a date equal to :date.',
    'date_format' => ':attribute 格式不正确, 例如 :format.',
    'different' => ':attribute 和 :other 不能一样 ',
    'digits' => ':attribute must be :digits digits.',
    'digits_between' => ':attribute must be between :min and :max digits.',
    'dimensions' => ':attribute has invalid image dimensions.',
    'distinct' => ':attribute 必须是唯一的',
    'email' => ':attribute 格式不正确.',
    'ends_with' => ':attribute 必须以 :values 结尾.',
    'exists' => '选择的 :attribute 不是有效值.',
    'file' => ':attribute 必须是一个文件.',
    'filled' => ':attribute 字段在模型中不是可填充的.',
    'gt' => [
        'numeric' => ':attribute must be greater than :value.',
        'file' => ':attribute must be greater than :value kilobytes.',
        'string' => ':attribute must be greater than :value characters.',
        'array' => ':attribute must have more than :value items.',
    ],
    'gte' => [
        'numeric' => ':attribute must be greater than or equal :value.',
        'file' => ':attribute must be greater than or equal :value kilobytes.',
        'string' => ':attribute must be greater than or equal :value characters.',
        'array' => ':attribute must have :value items or more.',
    ],
    'image' => ':attribute 必须是图片.',
    'in' => ':attribute 格式不正确.',
    'in_array' => ':attribute 格式不正确 :other.',
    'integer' => ':attribute 必须是整数.',
    'ip' => ':attribute 格式不正确(ip).',
    'ipv4' => ':attribute 格式不正确(ipv4).',
    'ipv6' => ':attribute 格式不正确(ipv6).',
    'json' => ':attribute 格式不正确(json).',
    'lt' => [
        'numeric' => ':attribute must be less than :value.',
        'file' => ':attribute must be less than :value kilobytes.',
        'string' => ':attribute must be less than :value characters.',
        'array' => ':attribute must have less than :value items.',
    ],
    'lte' => [
        'numeric' => ':attribute must be less than or equal :value.',
        'file' => ':attribute must be less than or equal :value kilobytes.',
        'string' => ':attribute must be less than or equal :value characters.',
        'array' => ':attribute must not have more than :value items.',
    ],
    'max' => [
        'numeric' => ':attribute 不能大于 :max.',
        'file' => ':attribute 不能大于 :max Kb.',
        'string' => ':attribute 不能大于 :max 字符.',
        'array' => ':attribute 最多选择 :max 项.',
    ],
    'mimes' => ':attribute 文件类型不正确: :values.',
    'mimetypes' => ':attribute 无效的文件类型 , 可选类型 :values.',
    'min' => [
        'numeric' => ':attribute 不能小于 :min.',
        'file' => ':attribute 不能小于 :min kb.',
        'string' => ':attribute 不能少于 :min 字符.',
        'array' => ':attribute 至少选中 :min 项.',
    ],
    'not_in' => 'selected :attribute is invalid.',
    'not_regex' => ':attribute format is invalid.',
    'numeric' => ':attribute 必须是整数',
    'password' => '密码不正确.',
    'present' => ':attribute 必须是百分比',
    'regex' => ':attribute 格式不正确',
    'required' => ':attribute 必填',
    'required_if' => '当 :other 的值为 :value 时，:attribute 是一个必填选项。',
    'required_unless' => ':attribute field is required unless :other is in :values.',
    'required_with' => ':attribute field is required when :values is present.',
    'required_with_all' => ':attribute field is required when :values are present.',
    'required_without' => ':attribute field is required when :values is not present.',
    'required_without_all' => ':attribute field is required when none of :values are present.',
    'same' => ':attribute and :other must match.',
    'size' => [
        'numeric' => ':attribute 大小必须为 :size.',
        'file' => ':attribute 大小必须为 :size Kb.',
        'string' => ':attribute 长度必须为 :size 个字符.',
        'array' => ':attribute 必须选中 :size 项.',
    ],
    'starts_with' => ':attribute 必须以 :values 开头.',
    'string' => ':attribute 必须是字符串.',
    'timezone' => ':attribute 必须是时区.',
    'unique' => ':attribute 已经被占用.',
    'uploaded' => ':attribute 已经上传了.',
    'url' => ':attribute 不是有效链接.',
    'uuid' => ':attribute 必须是一个 UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

];
