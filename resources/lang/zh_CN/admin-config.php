<?php
return [
	'labels' => [
		'AdminConfig' => '参数',
		'admin-config' => '参数',
	],
	'fields' => [
		'group' => '配置分组',
		'help' => '说明(Help)',
		'name' => '名称',
		'options' => '选项',
		'placeholder' => 'placeholder',
		'sort' => '排序',
		'status' => '状态',
		'title' => '标题',
		'type' => '类型',
		'value' => '配置值',
	],
	'options' => [
	],
];
