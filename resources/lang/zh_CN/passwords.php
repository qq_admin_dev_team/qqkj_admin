<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => '您的密码已重置 !',
    'sent' => '我们已向您的邮箱发送重置链接!',
    'throttled' => '请稍后重试.',
    'token' => '密码重置令牌已失效.',
    'user' => "无法找到该邮箱匹配的用户.",

];
