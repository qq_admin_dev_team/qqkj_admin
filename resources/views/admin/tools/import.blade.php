<label class="btn btn-primary btn-import " >
    <i class="fa fa-stack-overflow"></i>&nbsp;&nbsp;数据导入
</label>

<div id="upload-template" style="display: none">
    <div class="form-group row form-field attrs" >
        <label  class="col-md-2 text-capitalize control-label">下载模板</label>
        <div class="col-sm-8" style="padding-top:10px">
        @if ($url=='/order/import')
            <!--导入模板-->
                <a href="/uploads/order_import.xls" download="订单导入模板.xls">订单导入.xls</a>
        @endif
        @if ($url=='/goods/import')
            <!-- 商品导入模板-->
                <a href="/uploads/user_import.xls" download="商品导入模板.xls">商品导入.xls</a>
            @endif
        </div>
    </div>
    <div class="form-group row form-field attrs" >
        <label  class="col-md-2 text-capitalize control-label">选择文件</label>
        <div class="col-sm-8" style="padding-top:10px">
            <input type="file" name="files[]" class=" file-upload" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" >
        </div>
    </div>
    </label>
</div>
<script type="text/javascript">
    $(function(){
        $(".btn-import").unbind("click").bind("click",function(){
            layer.open({
                title:"数据导入",
                area:['500px','300px'],
                content:`<div id="import-modal">`+$("#upload-template").html()+`</div>`,
                yes:function(index){
                    var formData=new FormData();
                    formData.append("file", $("#import-modal .file-upload")[0].files[0]);
                    formData.append("_token","{{ csrf_token() }}");
                    $.ajax({
                        url:"{{admin_url($url)}}",
                        data:formData,
                        contentType: false,
                        processData: false,
                        type:"POST",
                        dataType:"json",
                        success:function(res){

                            if(res.code==200||res.code==0){
                                //导入成功
                            }else{
                                layer.open({
                                    title:"导入失败",
                                    content:res.msg
                                });
                            }
                        },
                        error(){

                        }
                    });
                    //清理已选择文件
                    $(".file-upload").val("");
                },
                success(){
                    console.log(123)
                }
            })
        })
    })
</script>
