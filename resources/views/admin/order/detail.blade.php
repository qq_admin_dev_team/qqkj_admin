<style type="text/css">
    .app-content{

        overflow-x: hidden;
        padding: 20px 20px
    }
    .main{
        background-color: #fff;
        min-height: 200px;
        border-radius: 10px;
        overflow: hidden;
        width:100%;
        margin-bottom: 30px;
    }
    .table td{
        height: auto;
        padding:15px 20px;
    }
    .order-detail{

        width:100%;
        height: 100%;
        background-color: #fff;
    }
    .img{
        width:50px;
        height: 50px;
    }
</style>
<div class="main">
    <table class=" table table-bordered ">
        <tr>
            <td>订单编号</td>
            <td>{{$order->order_no}}</td>
            <td>下单时间</td>
            <td>{{$order->created_at}}</td>
        </tr>
        <tr>
            <td>微信昵称</td>
            <td>{{$order->order_no}}</td>
            <td>微信头像</td>
            <td>{{$order->created_at}}</td>
        </tr>
        <tr>
            <td>订单金额</td>
            <td></td>
            <td>支付金额</td>
            <td>{{$order->created_at}}</td>
        </tr>
        <tr>
            <td>支付方式</td>
            <td></td>
            <td>支付时间</td>
            <td>{{$order->created_at}}</td>
        </tr>
        <tr>
            <td>发货时间</td>
            <td></td>
            <td>收货时间</td>
            <td>{{$order->created_at}}</td>
        </tr>
        <tr>
            <td>收货人</td>
            <td>{{$order->order_no}}</td>
            <td>联系方式</td>
            <td>{{$order->created_at}}</td>
        </tr>
        <tr>
            <td>收货地址</td>
            <td colspan="3"> {{$order->province}}{{$order->city}}{{$order->area}}{{$order->street}}</td>
        </tr>
        <tr>
            <td>买家留言</td>
            <td colspan="3"> {{$order->message}}</td>
        </tr>
    </table>
</div>
<div class="main">
    <table class=" table table-bordered ">
        <tr>
            <td>商品名</td>
            <td>商品图片</td>
            <td>购买数量</td>
            <td>小计</td>
        </tr>

            <tr>
                <td> </td>
                <td>
                    <img class="img" src="" >
                </td>
                <td>1</td>
                <td>998</td>
            </tr>
    </table>
</div>
<script type="text/javascript">
    $(function(){
        $(".order-detail:parent").find(".modal-dialog").eq(0).css("max-width",'1300px');
    })
</script>
