<style type="text/css">
    .center{text-align: center!important;}
    .panel{
        width:100%;background-color: #fff;padding:15px 20px  0px 20px;display: inline-block;vertical-align: top;
    }
    .panel-title{
        border-left:4px solid #ff6c9d;padding-left: 10px;
        margin-bottom: 15px;
    }
    .datas{
        height:120px;
        color:#fff;
        margin:0px 10px;padding-top:15px;
        text-align: center;
        box-shadow:0 1px 3px 1px rgba(34, 25, 25, 0.1)
    }
    .datas div{font-size: 13px;}
    .datas-data{font-size:46px!important;font-weight:300;}
    .datas-purple{
        background: linear-gradient(to right,#9C58C8,50%,#CB96D6);
    }
    .datas-blue{
        background: linear-gradient(to right,#3AA6D9,50%,#63BACB);
    }
    .datas-yellow{
        background: linear-gradient(to right,#F0A622,50%,#EBBB2A);
    }
    .datas-pink{
        background: linear-gradient(to right,#FD438F,50%,#FE6983);
    }
    .datas-green{
        background: linear-gradient(to right,#7BC69F,50%,#07C160);
    }
    .panel table td,.panel table th{
        padding:15px 15px;vertical-align: top;
        height: auto;
    }
    .panel table th{
        font-size: 16px;
    }
    .tips{
        color:#999;font-size: 12px;padding-left: 20px
    }
    .small{
        font-size: 11px;
    }
</style>
<script type="text/javascript" src="/vendor/js/echarts.min.js"></script>
<div style="background: #fff;padding-bottom: 40px;">
    <div class="panel">
        <div class="panel-title">数据统计</div>
        <div class="panel-abody row">
            <div class="col-md-3">
                <div class="datas datas-green">
                    <div class="datas-data">11<span class="small">人</span></div>
                    <div>用户总数</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="datas datas-yellow">
                    <div class="datas-data">￥88888<span class="small">元</span></div>
                    <div>累计交易额</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="datas datas-pink">
                    <div class="datas-data">￥1888<span class="small">元</span></div>
                    <div>本月交易额</div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="datas datas-blue">
                    <div class="datas-data">442<span class="small">笔</span></div>
                    <div>本月订单数</div>
                </div>
            </div>

        </div>
    </div>
    <div class="panel">
        <div class="panel-title">近30天趋势<span class="tips"></span></div>
        <div class="panel-abody center">
            <div id="main" style="width:100%;height:320px;"></div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-title">排行榜<span class="tips"></span></div>
        <div class="panel-abody center">
            <div class="row">
                <div class="col-md-7">
                    <table class=" table table-bordered ">
                        <tr>
                            <th colspan="3">畅销商品排行Top10</th>
                        </tr>
                        <tr>
                            <td >商品名</td>
                            <td>销量</td>
                            <td>金额</td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-5">
                    <table class=" table table-bordered ">
                        <tr>
                            <th colspan="3">会员消费排行Top10</th>
                        </tr>
                        <tr>
                            <td>昵称</td>
                            <td>订单数</td>
                            <td>消费金额</td>
                        </tr>
                        <tr>
                            <td>熏悟空</td>
                            <td>19</td>
                            <td>￥ 1024</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    Dcat.ready(function () {
        // js代码也可以放在模板里面
        var myChart = echarts.init(document.getElementById('main'));
        var option = {
            title:{
                text:'近30天趋势图',
                bottom:0,
                left:'center'
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    crossStyle: {
                        color: '#999'
                    }
                }
            },
            legend: {
                data: ['订单量', '成交额', '用户总数']
            },
            xAxis: [
                {
                    type: 'category',
                    data: ['10-01', '10-02', '10-03', '10-04', '10-05', '10-06', '10-07', '10-08', '10-09', '10-10', '10-11', '10-12'],
                    axisPointer: {
                        type: 'shadow'
                    }
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    name: '金额',
                    min: 0,
                    axisLabel: {
                        formatter: '{value} 元'
                    },
                    splitLine:{
                        show:false
                    }
                },
                {
                    type: 'value',
                    name: '订单数',
                    min: 0,
                    axisLabel: {
                        formatter: '{value} 单'
                    },
                    splitLine:{
                        show:false
                    }
                }
            ],
            series: [
                {
                    name: '订单量',
                    type: 'bar',
                    itemStyle:{
                        color:"#63BACB"
                    },
                    data: [20, 49, 70, 232, 256, 767, 1356, 1622, 326, 200, 64, 33]
                },
                {
                    name: '成交额',
                    type: 'bar',
                    itemStyle:{
                        color:"#FE6983"
                    },
                    data: [26, 59, 90, 264, 287, 707, 1756, 1822, 487, 188, 60, 23]
                },
                {
                    name: '用户总数',
                    type: 'line',
                    yAxisIndex: 1,
                    itemStyle:{
                        color:"#07C160",
                    },
                    data: [20, 22, 33, 45, 63, 102, 203, 234, 230, 165, 120, 62]
                }
            ]
        };
        myChart.setOption(option);
    });
</script>
