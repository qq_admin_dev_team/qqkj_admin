<style>

    .bootstrap-tagsinput {
        display: block;
        box-shadow: 0 1px 1px rgb(0 0 0 / 10%);
        padding:6px 15px;
        padding-bottom:3px;
    }
    .bootstrap-tagsinput .tag {
        color:#fff;
        background-color: #586cb1;
        display: inline-block;
    }
    .bootstrap-tagsinput .label {
        padding:6px 8px;
    }
</style>


<div class="{{$viewClass['form-group']}}">

    <div class="{{$viewClass['label']}} control-label">
        <span>{!! $label !!}</span>
    </div>
    <div class="{{$viewClass['field']}}">
        @include('admin::form.error')
        <div class="input-group">
            <input type="text" name="{{$name}}"  class="{{$class}}" value="{{ old($column, $value) }}"/>
        </div>
        @include('admin::form.help-block')
    </div>
</div>