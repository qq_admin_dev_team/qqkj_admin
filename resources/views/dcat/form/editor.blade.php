<div class="{{$viewClass['form-group']}}">

    <label class="{{$viewClass['label']}} control-label">{!! $label !!}</label>

    <div class="{{$viewClass['field']}}">

        @include('admin::form.error')

        <textarea class="form-control {{$class}}" name="{{$name}}" placeholder="{{ $placeholder }}" {!! $attributes !!} >{{ $value }}</textarea>

        @include('admin::form.help-block')

    </div>
</div>

<script require="@tinymce" init="{!! $selector !!}">
    var opts = {!! admin_javascript_json($options) !!};

    opts.selector = '#'+id;

    if (! opts.init_instance_callback) {

        opts.init_instance_callback = function (editor) {

            editor.on('Change', function(e) {
                var content = e.target.getContent();
                if (! content) {
                    content = e.level.fragments;
                    content = content.length && content.join('');
                }

                $this.val(String(content).replace('<p><br data-mce-bogus="1"></p>', '').replace('<p><br></p>', ''));
            });
        }
    }
    opts.file_picker_callback=function(callback, value, meta) {
        //文件分类
        var filetype='.pdf, .txt, .zip, .rar, .7z, .doc, .docx, .xls, .xlsx, .ppt, .pptx, .mp3, .mp4';
        //后端接收上传文件的地址
        var upurl='/admin/upload';
        //为不同插件指定文件类型及后端地址
        switch(meta.filetype){
            case 'image':
                filetype='.jpg, .jpeg, .png, .gif';
                break;
            case 'media':
                filetype='.avi, .flv, .mp4';
                break;
            case 'file':
            default:
        }
        //模拟出一个input用于添加本地文件
        var input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', filetype);
        input.click();
        input.onchange = function() {
            var file = this.files[0];

            var xhr, formData;
            console.log(file.name);
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            xhr.open('POST', upurl);
            xhr.onload = function() {
                var json;
                if (xhr.status != 200) {
                    Dcat.warning('HTTP Error: ' + xhr.status);
                    return;
                }
                console.log(xhr.responseText)
                json = JSON.parse(xhr.responseText);
                if (!json || typeof json.data.url != 'string') {
                    Dcat.warning('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                callback(json.data.url);
            };
            formData = new FormData();
            formData.append("_token","{{csrf_token()}}");
            formData.append('file', file, file.name );
            xhr.send(formData);
        };
    }
    tinymce.init(opts)
</script>
