# Dcat_admin 集成框架
## 本仓库不再维护，新版本迁移：**[dcat-faster](https://gitee.com/fitop/dcat-faster)** 
## 配置/文档
#### 集成插件
- **[Dcat_admin 备用文档](https://learnku.com/docs/dcat-admin/2.x)** 
- **[Laravel7 文档](https://learnku.com/docs/laravel/7.x)**
- **[Yansongda 微信支付和公众号支付](https://pay.yansongda.cn/docs/v2/)** 
- **[qiniu-laravel-storage 七牛云](https://github.com/zgldh/qiniu-laravel-storage/)**
- **[overtrue/easy-sms 短信插件](https://github.com/overtrue/easy-sms)**
- **[overtrue/pinyin 汉字转拼音](https://github.com/overtrue/pinyin)**
- **[overtrue/wechat 微信相关接口(5.1](https://easywechat.com/docs/5.x/overview)**
- **[easy-excel excel导入导出](https://github.com/jqhph/easy-excel)**
- **[intervention/image 图片处理](https://github.com/Intervention/image)**
- **[php-snowflake 雪花算法唯一ID](https://github.com/godruoyi/php-snowflake)**

####  安装步骤
- 拉取项目
- 配置nginx指向/public下，配置伪静态
- 导入数据库/database/init.sql，配置.env文件
- Linux服务器需要确保/storage目录和/public目录可写权限，如果要是使用代码生成工具需要整个项目写入权限
- 为了减小git包和源码包的体积，已经忽略vendor目录，先切换到项目目录，再运行：
~~~
	composer install
~~~

#### 后台账号：admin,密码：admin123
- **[演示站点1：（默认账号:admin 密码:admin123 ）](http://dcat.fit365.top/admin/auth/login)**

- **[官方演示站点（默认账号:admin 密码:admin ）](http://103.39.211.179:8080/admin/auth/login)**

#### 安装环境（必须）
- PHP7.2-7.4 （建议7.3)
- Nginx 1.15 +
- MySQL5.7-8.0 (建议8.0)
- 建议开启的php扩展
~~~
	OpenSSL
	fileinfo
	bcmath
	mb_string
	php_zip
	php_xmlreader
~~~


## 命令行
~~~
#生成工具表单# 
php artisan admin:form UserSetting --title=用户设置
#生成控制器 #
php artisan admin:make PostController --model=App\Post
更多命令行运行 php artisan 参看
~~~

## 图片/文件上传
~~~ 
	/api/attachment/upload   //api上传，如小程序端 
	/admin/upload   //后端上传
	
	后台示例：
	//单图表单上传 
	$form->image('avatar',"商品缩略图")
		->accept('jpg,png,gif,jpeg,bmp,jpg', 'image/*')
		->disableRemove()
		->url("upload");
	//多图表单上传
	$form->multipleImage('images', '商品轮播图')->limit(5)
		->accept('jpg,png,gif,jpeg,bmp,jpg', 'image/*') 
		->url("upload")
		->saving(function ($paths) {
			return implode(',', $paths);//指定用逗号拼接保存
		});

	//列表预览，单图多图通用
	$grid->column('images')->display(function ($pictures) {
		return explode(",", $pictures);//指定用逗号分割
			//  return json_decode($pictures, true);
	})->image(picture_server(), "/"), 50, 50);
~~~  

## Excel导入、导出
- 工具类封装：\App\Admin\Grid\ImportTool 和 \resources\views\admin\tools\import.blade.php

#### Excel导入
~~~

use App\Admin\Grid\ImportTool;
use Dcat\Admin\Traits\HasUploadedFile;
use Dcat\EasyExcel\Excel; 

class UserController extends AdminController {
	use HasUploadedFile;
	protected function import(Request $request) {
		//先保存到本地
		$disk = $this->disk("local");
		if (!$request->hasFile('file')) {
			return show(500, "未检测到文件");
		}
		$file = $request->file("file");
		$newName = date("Ym") . "/" . md5(uniqid() . mt_rand(1000, 9999)) . "." . $file->getClientOriginalExtension();
		$result = $disk->putFileAs("uploads/files", $file, $newName);
		$path = "/uploads/files/" . $newName;
		$sheet = Excel::import(public_path($path))->sheet(0)->toArray();
		var_dump($sheet);
	}
}
~~~
#### Excel导出
- 自定义导出用easy-excel
- grid列表快速导出
~~~ 
	$grid->export()->rows(function (array $rows) { 
		return $rows;
	}); 
~~~

## 支付调用

~~~ 
	use Pay;
	/**
	 * 微信小程序支付
	 * @return {json} [返回微信小程序支付签名包]
	 */
	function miniapp_pay(Request $requset){
		$wechat = Pay::wechat(config("pay.wechat"));
		$order = [
			'out_trade_no' => (new Snowflake())->id(),
			'body' => "小程序支付测试",
			'openid' => "xxxxxx",
			'total_fee' => 1,
		];

		return Pay::wechat(config("pay.wechat"))->miniapp($order);
	}

	/**
	 * 微信支付回调
	 */
	function wxpay_back(Request $requset) {
		$wechat = Pay::wechat(config("pay.wechat"));
		$result = $wechat->verify(); 
		if (isset($result->out_trade_no)) {
			//根据单号查询并更新数据库
		} 
		return $wechat->success()->send();
	}
~~~ 
### 支付回调
~~~
	/**
	 * 微信支付回调
	 */
	function wxpay_back(Request $requset) {
		$wechat = Pay::wechat(config("pay.wechat"));
		$result = $wechat->verify(); 
		if ($result&&isset($result->out_trade_no)) {
			//根据单号查询并更新数据库
		} 
		return $wechat->success()->send();
	}
	/**
	 * 支付宝支付回调
	 */
	function alipay_back(Request $request) {
		$alipay = Pay::alipay(config("pay.alipay"));
		$result = $alipay->verify();
		if (isset($result->out_trade_no)) {
			//根据单号查询并更新数据库
		}
		return $alipay->success();
	}
~~~
## 小程序、公众号接口
#### 微信小程序获取openid
~~~
	use EasyWeChat\Factory;
 
	function miniapp_oauth(Request $request) {
		$config = config("wechat.mini_program.default");
		$app = Factory::miniProgram($config); 
		try {
			$res = $app->auth->session($request->input("code", ''));
			if (isset($res['openid'])) {
				//session_key建议不要返回给前端
				session("session_key", $res['session_key']);
				return json_msg(0, "微信登录", $res['openid']);
			} else {
				return json_msg(0, "授权登录失败", $res);
			}
		} catch (\Exception $e) {
			//配置错误时排除异常
			return json_msg(500, $e->getMessage());
		}

	}
~~~
#### 公众号授权
~~~
/**
	use EasyWeChat\Factory;

	function official_oauth(Request $request) {
		$config = config("wechat.official_account.default");
		$app = Factory::officialAccount($config);

		//静默授权
		/*$response = $app->oauth->scopes(['snsapi_base'])
			->setRequest($request)
			->redirect(request_domain() . "/api/demo/oauth_back");*/
		//用户信息授权
		$response = $app->oauth->scopes(['snsapi_userinfo'])
			->setRequest($request)
			->redirect(request_domain() . "/api/demo/oauth_back");
		return $response;
	}
	/**
	 * 公众号授权回调
	 * @param  Request $request [description] 
	 */
	function oauth_back(Request $request) {
		$config = config("wechat.official_account.default"); 
		$app = Factory::officialAccount($config);
		$user = $app->oauth->user(); 
		
		$user->getId();  // 对应微信的 OPENID
		$user->getNickname(); // 对应微信的 nickname
		$user->getAvatar(); // 头像网址
		$user->getOriginal(); // 原始API返回的结果
		$user->getToken(); // access_token， 比如用于地址共享时使用 
	}
~~~
#### 公众号模板消息推送
~~~
/**
	use EasyWeChat\Factory;

	function official_push(Request $request) {
		$config = config("wechat.official_account.default");
		$app = Factory::officialAccount($config);

		$res = $app->template_message->send([
			'touser' => 'oUN1NxLNoCzq-B09a0FVYZ1iqLcM',//公众号推送
			'template_id' => 'yz673S53td8r86tVqxsd-LPRzpxbo_lnRCAxM4qIPvs',
			'url' => 'https://easywechat.org',
			'data' => [
				'first' => "恭喜您下单成功！",
				'keyword1' => $this->uuid(),
				'keyword2' => date("Y-m-d H:i"),
				'keyword3' => "100元",
				'remark' => "我们会尽快发货",
			],
		]);
		var_dump($res);
	}
~~~
#### 公众号模板消息推送
~~~
/**
	use EasyWeChat\Factory;

	function official_push(Request $request) {
		$config = config("wechat.official_account.default");
		$app = Factory::officialAccount($config);

		$res = $app->template_message->send([
			'touser' => 'oUN1NxLNoCzq-B09a0FVYZ1iqLcM',//公众号推送
			'template_id' => 'yz673S53td8r86tVqxsd-LPRzpxbo_lnRCAxM4qIPvs',
			'url' => 'https://easywechat.org',
			'data' => [
				'first' => "恭喜您下单成功！",
				'keyword1' => $this->uuid(),
				'keyword2' => date("Y-m-d H:i"),
				'keyword3' => "100元",
				'remark' => "我们会尽快发货",
			],
		]);
		var_dump($res);
	}
~~~

## 改动说明


## app/helper.php 公共助手函数文件
#### show(),show_success(),show_error() 接口统一返回
~~~
    show(403,'暂无修改权限',$data);   //自定义code返回
    show_success('操作成功',$data);  //快速返回成功，code为200，
    show_error('操作失败',$data);    //快速返回失败信息，code为500
~~~
#### admin_config() 获取配置
- 获取后台的表单关联后台的分组配置表单，此方法会用缓存
- 后台菜单/admin/config/settings，/admin/config/items，/app/Admin/Forms/setting.php
~~~
    admin_config("withdraw_percent");
~~~
#### unique_no() 快速生成唯一单号
~~~
    unique_no();    //年份开头的20位单号
    unique_no("TK",mt_rand(1000,9999)); //追加自定义前缀和后缀
~~~
#### 生成数字唯一ID 和 UUID
~~~
    unique_bigint(); //雪花算法 16位数字，如果保存数据库建议用varchar而不是bigint (可能会造成接口返回时整数溢出)
    uuid(); //生成laravel提供的 uuid
~~~
#### validate_form() 快速验证用户提交数据
- 
~~~
    validate_form([
        "mobile|手机号" => "required|size:11",
        "name|姓名" => "required"
    ]);
~~~
#### children_tree() 递归构造二叉树
~~~
    $list=Category::->select('id','pid','name')get()->toArray();
    $list=children_tree($list);
~~~
#### curl_post(),curl_get() 发送CURL请求
~~~
    curl_post($url,['name'=>'G'],['ContentType:application/json']);
    curl_get($url); 
~~~
#### rand_char() 生成随机字符串，数字
~~~
    rand_char(8,5);
~~~
#### number2chinese() 数字转汉字
~~~
    number2chinese(25); // 25 > 二十五
~~~
#### distance_api(),distance_line() 获取两地点的距离
~~~
    distance_api($lat1,$long1,$lat2,$long2,'walking'); //腾讯地图接口
    distance_line($lat1,$long1,$lat2,$long2);       //球体计算距离
~~~
#### send_sms() 发送阿里云短信
- 需要在.env 和/config/easysms.php配置短信
~~~
    send_sms("notice",185xxxx2333,['order_no'=>'xxxxx','status'=>'已发货']);//发送通知类短信
    send_sms("login",185xxxx2333,['code'=>mr_rand(100000,999999)],true);//发送登录验证码并缓存验证码
    check_sms_code("login",185xxxx2333,$code);//校验验证码
~~~
#### 敏感数据加/解密
~~~
    aes_decrypt($decryptedString);
    aes_encrypt($array);
~~~
## 框架改动 /vendor
注意：以下内容用composer重新安装vendor目录后失效!!!

- 增加底部版权，文件：\vendor\dcat\laravel-admin\resources\views\layouts\vertical.blade.php
- 修改富文本组件 ，增加了视频，文件上传，文件：\vendor\dcat\laravel-admin\resources\views\form\editor.blade.php


##  多后台/多应用/多API
- 多后台配置 ：http://www.dcatadmin.com/docs/master/multi-app.html
- 多API配置 修改\app\Providers\RouteServiceProvider.php ,增加路由映射和新的路由文件
~~~
	protected function mapApiRoutes() {
		Route::prefix('api')
			->middleware('api')
			->namespace($this->namespaceapi)
			->group(base_path('routes/api.php'));
		Route::prefix('apiv2')
			->middleware('api')
			->namespace("App\Http\Apiv2\Controllers")
			->group(base_path('routes/apiv2.php'));
	}
~~~
	


 



